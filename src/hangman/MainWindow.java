package hangman;

import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class MainWindow extends JFrame
{
	private JLabel spaceForEachLetter;
	private JLabel GuessesLeft;
	private JTextField lettersEntered;
	private int remainingGuesses = 10;
	private String wrongGuesses = "";
	private String word;
	private String visible="_";
	
	public MainWindow(String toGuess)
	{
		word = toGuess;
		setSize(300, 200);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setTitle("Hangman");
		setLayout(new GridLayout(4,1));
		
		JPanel panel1 = new JPanel();
		add(panel1);
		GuessesLeft = new JLabel("You have 10 Guesses Remaining");
		panel1.add(GuessesLeft);
		
		JPanel panel2 = new JPanel(new FlowLayout());
		add(panel2);
		for(int i=0;i<word.length()-1;i++)
		{
			visible = visible +" _";
			
		}
		spaceForEachLetter = new JLabel("<html><tag><h2><b>"+visible+"</b></h2></tag></html>");
		panel2.add(spaceForEachLetter);
		JPanel panel3 = new JPanel();
		add(panel3);
		lettersEntered = new JTextField(word.length());
		panel3.add(lettersEntered);
		
		JPanel panel4 = new JPanel();
		add(panel4);
		JLabel wrongGuesses = new JLabel("Wrong guesses so far: ");
		panel4.add(wrongGuesses);
	}
	
}
