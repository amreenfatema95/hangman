package hangman;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Random;

public class Main 
{
	//private static String[] words = {"goldfish","rabbit","computer","rain","forecast","hangman"};
	private static String[] words;
	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		try {
			BufferedReader dict = new BufferedReader(new FileReader("dictionary.txt"));
			int numberOfLines = Integer.parseInt(dict.readLine());
			words = new String[numberOfLines];
			for(int i =0;i<numberOfLines;i++)
			{
				words[i] = dict.readLine();
			}
		} 
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String randomWord = (words[new Random().nextInt(words.length)]);
		System.out.println(randomWord);
		MainWindow test = new MainWindow(randomWord);
		test.setVisible(true);
	}

}
